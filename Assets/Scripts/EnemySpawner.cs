﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemy;
    public float xPos, yPos;
    public int enemyCount;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(EnemySpawn());
    }

    IEnumerator EnemySpawn()
    {
        while (true)
        {
            if (enemyCount < 10)
            {
                //player position x + Random +-5
                GameObject player = GameObject.FindGameObjectWithTag("Player");
                if (player != null)
                {
                    Vector2 position = player.transform.position;
                
                    xPos = position.x + Random.Range(-5, 5);
                    if (xPos > 10)
                    {
                        xPos = 10;
                    }
                    else if (xPos < -10)
                    {
                        xPos = -10;
                    }
                
                    yPos = position.y + Random.Range(-5, 5);
                    if (yPos > 10)
                    {
                        yPos = 10;
                    }
                    else if (yPos < -10)
                    {
                        yPos = -10;
                    }

                }

                Instantiate(enemy, new Vector3(xPos, yPos, 0), Quaternion.identity);

                enemyCount++;
            }
                
            yield return new WaitForSeconds(1);
        }
    }

    public void EnemyDestroyed()
    {
        if (enemyCount > 0)
        {
            enemyCount--;
        }
    }

}
