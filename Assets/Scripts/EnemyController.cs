﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    Rigidbody2D rigidbody2D;
    public float speed;
    public float changeTime = 3.0f;
    int direction = 1;
    float timer;
    bool vertical;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        timer = changeTime;
        vertical = Random.value > 0.5f;
    }

    void FixedUpdate()
    {
        timer -= Time.deltaTime;

        if (timer < 0)
        {
            direction = -direction;
            timer = changeTime;
        }

        Vector2 position = rigidbody2D.position;


        if (vertical)
        {
            position.y = position.y + direction * speed * Time.deltaTime;
        }
        else
        {
            position.x = position.x + direction * speed * Time.deltaTime;
        }

        rigidbody2D.MovePosition(position);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.GetComponent<EnemySpawner>().EnemyDestroyed();
            Destroy(gameObject);
        }
    }

}
